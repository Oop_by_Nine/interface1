/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.interface1;

/**
 *
 * @author Nine
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("Bat");
        Plane plane = new Plane("Engine number one");
        bat.fly(); // Animal, Poultry, Flyable
        plane.fly(); // Vahicle, Flyable
        Dog dog = new Dog("Namtan");
        Car car = new Car("Engine number one");

        Flyable[] flyables = {bat, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }

        Runable[] runables = {dog, plane, car};
        for (Runable r : runables) {
            r.run();

        }

    }

}


