/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.interface1;

/**
 *
 * @author Nine
 */
public class Plane extends Vahicle implements Flyable, Runable {

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: Start engine");

    }

    @Override
    public void stopEngine() {

    }

    @Override
    public void raiseEngine() {

    }

    @Override
    public void applyEngine() {

    }

    @Override
    public void fly() {
        System.out.println("Plane: fly");

    }

    @Override
    public void run() {
        System.out.println("Plane: run");
    }

}

